# Kibana init #

This is LSB-compliant init.d script for Kibana 4.

### Installing ###

    sudo mv kibana /etc/init.d
    sudo chmod 755 /etc/init.d/kibana
    sudo chkconfig --add kibana

### Prerequisites ###

`/lib/lsb/init-functions`

### Configuration ###

Kibana installation directory is set to `/opt/kibana` by default. It can be customized with `KIBANA_HOME_DIR`.  
Default logs location: `/var/log/kibana/kibana.log`.  
See other options in a script inside `Settings`.  